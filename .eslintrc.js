module.exports = {
  env: {
    es6: true,
    node: true
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint'
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    // Possible Errors
    'no-await-in-loop': 'error',
    'no-debugger': 'error',
    'no-template-curly-in-string': 'error',

    // Best Practices
    'no-multi-spaces': 'error',
    'wrap-iife': ['error', 'inside'],
    'no-redeclare': 'error',
    'eqeqeq': ['error', 'always'],
    'require-await': 'error',
    'no-return-await': 'error',
    'no-constructor-return': 'error',
    'no-else-return': 'error',
    'no-eq-null': 'error',
    'no-fallthrough': 'error',
    'no-implicit-coercion': 'error',
    'no-invalid-this': 'error',
    'no-lone-blocks': 'error',
    'no-new': 'error',
    'no-return-assign': 'error',
    'no-sequences': 'warn',
    'vars-on-top': 'warn',
    'yoda': ['error', 'never'],
    'curly': ['error', 'all'],

    // Variables
    'no-undefined': 'error',
    'no-use-before-define': 'error',

    // Stylistic Issues
    'semi': ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    'no-trailing-spaces': 'error',
    'eol-last': ['error', 'always'],
    'indent': ['error', 2, {
      VariableDeclarator: 'first',
      ignoredNodes: [
        'FunctionExpression > .params[decorators.length > 0]',
        'FunctionExpression > .params > :matches(Decorator, :not(:first-child))',
        'ClassBody.body > PropertyDefinition[decorators.length > 0] > .key'
      ]
    }],
    'quotes': ['error', 'single', {
      avoidEscape: true,
      allowTemplateLiterals: true
    }],
    'spaced-comment': ['error', 'always', {
      line: {
        markers: ['#region', '#endregion', 'region', 'endregion']
      }
    }],
    'capitalized-comments': ['error', 'always', {
      ignoreConsecutiveComments: true,
      ignorePattern: '#region|#endregion|region|endregion'
    }],
    'brace-style': ['error', 'stroustrup', {
      allowSingleLine: false
    }],
    'block-spacing': 'error',
    'space-in-parens': ['error', 'never'],
    'func-call-spacing': ['error', 'never'],
    'key-spacing': ['error', {
      beforeColon: false,
      afterColon: true
    }],
    'space-before-function-paren': ['error', {
      anonymous: 'always',
      named: 'never',
      asyncArrow: 'always'
    }],
    'object-curly-spacing': ['error', 'always', {
      arraysInObjects: true,
      objectsInObjects: true
    }],
    'quote-props': ['error', 'consistent-as-needed'],

    // ES6
    'arrow-parens': ['error', 'as-needed'],
    'no-var': 'error',
    'prefer-const': 'error',
    'arrow-spacing': ['error', {
      before: true,
      after: true
    }],
    'no-duplicate-imports': ['error', { includeExports: true }],
    'prefer-template': 'warn'
  },
  overrides: [
    {
      files: ['tests/**/*'],
      rules: {
        'no-undef': 'off',
        'no-await-in-loop': 'off'
      }
    }
  ]
}
